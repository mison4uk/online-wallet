@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Управление кошельками</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form method="post" action="/wallet">
                            {{ csrf_field() }}
                            <h5>Кошельки пользователя:</h5> <br><br>
                            @foreach($wallet as $key => $value)
                              <h6>Кошелек - {{$key}}, валюта - {{$value}}</h6><br>
                            @endforeach
                            <br>
                            <br>
                            <button type="submit">Добавить кошелек</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
