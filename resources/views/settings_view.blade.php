@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Настройки приложения</div>



                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form method="post" action="/settingsSave">
                            {{ csrf_field() }}

                            <label>Валюта по умолчанию:</label>
                            <select name="currency">
                                @foreach($currency as $value)
                                    <option value="{{$value->id}}">{{$value->currency}}</option>
                                @endforeach
                            </select>


                            <br><br>
                            <button type="submit">Записать</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
