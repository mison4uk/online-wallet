@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Добавление кошелька</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form method="post" action="/addWallet">
                            {{ csrf_field() }}
                            Название кошелька: <input type="text" name="wallet"> <br><br>
                            <label>Валюта:</label>
                            <select name="currency">
                                @foreach($currency as $value)
                                <option value="{{$value->id}}">{{$value->currency}}</option>
                                @endforeach
                            </select>
                            <br><br>
                            <button type="submit">Записать</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
