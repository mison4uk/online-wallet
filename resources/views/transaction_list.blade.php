@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Список транзакций</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form method="post" action="/transactionView">
                            {{ csrf_field() }}

                            @foreach($transactions as $value)

                                @if($value[0] == 0)
                                    {{$type_transaction[0]}} -
                                @else
                                    {{$type_transaction[1]}} -
                                @endif

                                кошелек({{$value[4]}}) - {{$value[1]}}грн. ({{$value[2]}}) - {{$value[3]}}<br>

                            @endforeach
                            <br><br>
                            <button type="submit">Добавить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
