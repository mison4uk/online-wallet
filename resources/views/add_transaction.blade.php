@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Создание транзакции</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form method="post" action="/addTransaction">
                            {{ csrf_field() }}
                            <label>Дата транзакции:</label>
                            <input type="date" name="date">
                            <br>
                            <br>
                            <label>Тип транзакции:</label>
                            <select name="type_transaction">
                                @foreach($type_transaction as $key => $value)
                                <option value="{{$key}}">{{$value}}</option>
                                @endforeach
                            </select>
                            <br>
                            <br>
                            <label>Кошелек:</label>
                            <select name="wallet">
                                @foreach($wallet as $value)
                                <option value="{{$value->id}}">{{$value->wallet_name}}</option>
                                @endforeach
                            </select>
                            <br>
                            <br>
                            <label>Сумма:</label>
                            <input type="text" name="suma">
                            <br>
                            <br>
                            <label>Описание:</label>
                            <input type="text" name="description">
                            <br>
                            <br>
                            <button type="submit">Записать</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
