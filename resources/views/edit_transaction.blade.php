@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Редактирование транзакции</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                            <form method="post" action="/addWallet">
                                {{ csrf_field() }}
                                <label>Дата транзакции:</label>
                                <br>
                                <input type="date" name="date">
                                <br>
                                <label>Тип транзакции:</label>
                                <br>
                                <select name="type_transaction">
                                    <option value=""></option>
                                </select>
                                <br>
                                <label>Кошелек:</label>
                                <br>
                                <select name="wallet">
                                    <option value=""></option>
                                </select>
                                <br>
                                <label>Сумма:</label>
                                <br>
                                <input type="text" name="suma">
                                <br>
                                <label>Описание:</label>
                                <br>
                                <input type="text" name="description">
                                <br>
                                <br>
                                <button type="submit">Записать</button>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
