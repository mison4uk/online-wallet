<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::match(['get', 'post'], '/wallet', 'WalletController@view')->middleware('auth');

Route::get('/wallets', 'WalletController@wallets')->middleware('auth');

Route::match(['get', 'post'], '/addWallet', 'WalletController@addWallet')->middleware('auth');

Route::match(['get', 'post'], '/addTransaction', 'TransactionController@addTransaction')->middleware('auth');

Route::match(['get', 'post'], '/transactionList', 'TransactionController@transactionList')->middleware('auth');

Route::match(['get', 'post'], '/transactionView', 'TransactionController@transactionView')->middleware('auth');

Route::match(['get', 'post'], '/settingsView', 'SettingsController@settingsView')->middleware('auth');

Route::match(['get', 'post'], '/settingsSave', 'SettingsController@settingsSave')->middleware('auth');
