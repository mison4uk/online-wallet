<?php


namespace App\Http\Controllers;


use App\Models\Setting;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SettingsController extends Controller
{

    public function settingsView()
    {
        $currency = Db::table('currencies')->select('id', 'currency')->get();
        $array = $currency->toArray();


        return view('settings_view', [
            'currency' => $array
        ]);

    }


    public function settingsSave()
    {
        $setting = Setting::where('user_id', Auth::user()->id)->first();

        if (isset($setting)){

            if ($setting->key == 'currency'){
                $setting->user_id = Auth::user()->id;
                $setting->key = 'currency';
                $setting->value = $_POST['currency'];
                $setting->save();
            } else {
                $setting->user_id = Auth::user()->id;
                $setting->key = 'currency';
                $setting->value = $_POST['currency'];
                $setting->save();
            }
        } else {
            $setting = new Setting();
            $setting->user_id = Auth::user()->id;
            $setting->key = 'currency';
            $setting->value = $_POST['currency'];
            $setting->save();
        }

        echo 'Валюта установлена по умолчанию';
        return $this->settingsView();
    }

}
