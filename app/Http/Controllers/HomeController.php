<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $record;

    public $email;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
       $email = $request->user()->email;

       $this->record = Db::table('users')->where('email', $email)->first();

       $name = $this->record->name;

       $mail = $this->record->email;

       return view('home', [
           'name' => $name,
           'mail' => $mail
       ]);
    }
}
