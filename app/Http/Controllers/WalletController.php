<?php


namespace App\Http\Controllers;


use App\Models\Currency;
use App\Models\Wallet;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class WalletController extends Controller
{

    public function view()
    {

        $currency = Db::table('currencies')->select('id', 'currency')->get();
        $array = $currency->toArray();

        return view('wallet', [
            'currency' => $array
        ]);

    }

    public function addWallet()
    {

        if(isset($_POST)){

            $wallet = new Wallet();
            $wallet->user_id = Auth::user()->id;
            $wallet->wallet_name = $_POST['wallet'];
            $wallet->currency_id = $_POST['currency'];
            if ($wallet->save()){
                echo 'Кошелек "' . $_POST['wallet'] . '" удачно добавлен';
            }
        }
        return $this->wallets();
    }

    public function wallets()
    {
        $mas = [];

        $user = User::where('id', Auth::user()->id)->first();
        $wallet = $user->wallets;

        foreach ($wallet as $value){
            $currency = Currency::where('id', $value->currency_id)->first();
            $currency_wallet = $currency->currency;
            $mas[$value->wallet_name] = $currency_wallet;

        }

        return view('wallet_view', [
            'wallet' => $mas
        ]);
    }
}
