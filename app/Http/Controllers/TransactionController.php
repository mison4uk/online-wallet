<?php


namespace App\Http\Controllers;


use App\Models\Transaction;
use App\Models\Wallet;
use App\User;
use Illuminate\Support\Facades\Auth;

class TransactionController extends Controller
{

    public $type_transaction = [
        'расход',
        'доход'
        ];


    public function transactionList()
    {

        $arr = [];

        $transactions = Transaction::all();


        foreach ($transactions as $value){

            $transaction = Transaction::where('id', $value->id)->first();
            $wallet = $transaction->wallet->wallet_name;
            $transaction->all();
            if ($transaction->user_id == Auth::user()->id) {

                $arr[] = [
                    $transaction->transaction_type,
                    $transaction->summa,
                    $transaction->description,
                    $transaction->transaction_date,
                    $wallet
                ];
            }
        }

        return view('transaction_list', [
            'transactions' => $arr,
            'type_transaction' => $this->type_transaction
        ]);
    }

    public function transactionView()
    {
        $user = User::where('id', Auth::user()->id)->first();
        $wallet = $user->wallets;

        return view('add_transaction', [
            'wallet' => $wallet,
            'type_transaction' => $this->type_transaction

        ]);

    }

    public function addTransaction()
    {

        if (isset($_POST)){
            $transaction = new Transaction();
            $transaction->user_id = Auth::user()->id;
            $transaction->wallet_id = $_POST['wallet'];
            $transaction->transaction_type = $_POST['type_transaction'];
            $transaction->transaction_date = $_POST['date'];
            $transaction->summa = $_POST['suma'];
            $transaction->description = $_POST['description'];
            if ($transaction->save()) {
                echo 'Транзакция успешно добавлена';
            }
        }

        return $this->transactionList();
    }
}
