<?php


namespace App\Providers;


use App\Services\Wallet;
use Illuminate\Support\ServiceProvider;


class WalletServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('wallets', function($app){
            return new Wallet();
        });
    }
}
